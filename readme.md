# <ai-interface/>
Experimental web components to:
1. record HTML`Audio` from a web page; accept text input
   (HTML`textarea` and `URLSearchParams` `#input=`)
2. send the audio to openAI, so from a audio speech we get a text transcription
3. print the text transciption to the screen, or/and make the computer speak a audio version
4. send the user "transciption" to openAI chat gpt for "completion"
5. display the resulting "user/chat gpt log"

You will need a **private** _openai-api-key_ found in https://platform.openai.com/account/api-keys.

> Warning: this **private** API key is saved in the brwoser local
> storage, which should be secure; but might not be enough for the
> sentitivity of your content.

# Usage
- on the website https://attention1.gitlab.io/ai-interface/ (fork to
  deploy a personal instance)
- as a npm package `@attention1/ai-interface`
- support on [#ai-interface:matrix.org](https://matrix.to/#/#ai-interface:matrix.org)

## As a npm package
To install:
```bash
npm install --save @attention1/ai-interface
```

## From the URL
It seems openAI chat GPT has currently no way to be queried from the URLs.

So adding some tests here.
```txt
https://attention1.gitlab.io/ai-interface/#input=<user_query>
```

It should be then possible to use it with
https://github.com/internet4000/find/ and the query `!ai <query>`.

It uses "url Hash params" which should not be shared by the brwoser to
the server hosting the "ai-interface" code.

The user input is shared via the `#` param, only to the web browser
(the user typed it there), and the opened application (ai-interface).

# Development
To develop with a local server:
```bash
npm install
npm run dev
```

The web-components are written with https://lit.dev and the OpenAi and
Browser APIs.
