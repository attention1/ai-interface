import { LitElement, html } from 'lit';

export default class AudioRecorder extends LitElement {
	static get properties() {
		return {
			submit: { type: String },
			recording: { type: Boolean },
			audioChunks: { type: Array },
			mimeType: { type: String },
		};
	}
	getText() {
		const text = this.submit || "record"
		if (this.recording) {
			return `Stop ${text}`
		} else {
			return `Start ${text}`
		}
	}

	constructor() {
		super();
		this.recording = false;
		this.audioChunks = [];
		/* ['m4a', 'mp3', 'webm', 'mp4', 'mpga', 'wav', 'mpeg'] */
		/* this.mimeType = 'audio/webm; codecs=opus'; */
		if (MediaRecorder.isTypeSupported('audio/mp3')) {
			this.mimeType = 'audio/mp3'
		} else if (MediaRecorder.isTypeSupported('audio/webm')) {
			this.mimeType = 'audio/webm'
		}
	}

	render() {
		return html`
			<button @click=${this._toggleRecording} ?active=${!!this.recording}>
				${this.getText()}
			</button>
			<input type="file" @change="${this._handleFileInputChange}" />
		`;
	}

	_handleFileInputChange(event) {
		const files = event.target.files;
		if (!files || files.length === 0) {
			return;
		}
		const file = files[0];
		this.dispatchEvent(new CustomEvent('audio', { detail: file }));
	}

	_toggleRecording() {
		if (this.recording) {
			this._stopRecording();
		} else {
			this._startRecording();
		}
	}

	async _startRecording() {
		const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
		this.mediaRecorder = new MediaRecorder(stream, { mimeType: this.mimeType });
		this.mediaRecorder.addEventListener('dataavailable', (event) => {
			if (event.data.size > 0) {
				this.audioChunks.push(event.data);
			}
		});
		this.mediaRecorder.addEventListener('stop', () => {
			const audioBlob = new Blob(this.audioChunks, { type: this.mimeType });
			const file = new File([audioBlob], `recording-${Date.now()}.webm`, {
				type: this.mimeType
			});
			this.dispatchEvent(new CustomEvent('audio', { detail: file }));
		});
		// if started with a number arguments, break mime-type
		this.mediaRecorder.start();
		this.recording = true;
	}

	_stopRecording() {
		this.recording = false;
		this.mediaRecorder.stop();
		this.audioChunks = []
	}
	createRenderRoot() {
		return this
	}
}
