import { LitElement, html } from 'lit';
import './audio-recorder.js';
import './speech-to-text.js';

export default class AIInterface extends LitElement {
	static get properties() {
		return {
			apiKey: { type: String, attribute: 'openai-api-key', reflect: true },
			recording: { type: Object, reflect: true, state: true},
			transcription: { type: String, reflect: true, state: true },
			logs: { type: Array, reflect: true, state: true },
			autoAskGPT: { type: Boolean, reflect: true, state: true },
			speak: { type: Boolean, reflect: true, state: true },
		};
	}

	/* a Log, is a "text chat request to gpt";
		 (might come from an audio file, listed for source checking) */
	_newLog({
		audioInput,
		transcription,
		error
	}) {
		const log = {
			audioInput,
			transcription,
			error,
			id: self.crypto.randomUUID(),
		}
		this.logs = [...this.logs, log]
	}

	/* gets the user input and app config from URL params; */
	_getUrlParams() {
		let data
		try {
			data = new URLSearchParams(window.location.hash.slice(1))
		} catch(e) {}
		return {
			input: data.get('input')
		}
	}
	_clearUrlParam(name) {
		const params = new URLSearchParams(window.location.hash.slice(1))
		params.delete(name)
		if (params.size) {
			window.location.hash = params.toString()
		} else {
			window.location.hash = ''
		}
	}
	constructor() {
		super();
		this.settings = null;
		this.apiKey = '';
		this.recording = null;
		this.transcription = '';
		this.logs = [];
		this.autoAskGPT = false;
		this.speak = false;
		this._addEventListeners()
	}
	_addEventListeners() {
		window.addEventListener("hashchange", this._handleHashChange.bind(this));
	}

	connectedCallback() {
		super.connectedCallback()
		if (this.settings) {
			if (this.settings['openai-api-key']) {
				this.apiKey = this.settings['openai-api-key']
			}
			if (this.settings['auto-ask-gpt']) {
				this.autoAskGPT = this.settings['auto-ask-gpt']
			}
			if (this.settings['speak']) {
				this.speak = this.settings['speak']
			}
		}
		/* get the user input from url hash search params;
		 it shoult stay local to the brwoser; and nver go to the server */
		const {input} = this._getUrlParams()
		if (input) {
			this._handleInitialInput(input)
		}
	}

	render() {
		return html`
			<header>
				<app-settings
					@settings=${this._handleSettings}
					></app-settings>
			</header>
			<main>
				<log-list
					.logs=${this.logs}
					.apiKey=${this.apiKey}
					.autoAskGPT=${this.autoAskGPT}
						speak=${this.speak}
					></log-list>
			</main>
			<footer>
				<text-recorder @text=${this._handleTextRecorded} submit="➹"
					></text-recorder>
				<audio-recorder @audio=${this._handleAudioRecorded} submit="🎙"
					></audio-recorder>
				<speech-to-text @transcription=${this._handleTranscription}
					.apiKey=${this.apiKey}
					></speech-to-text>
			</footer>
		`;
	}
	_handleHashChange() {
		const {input} = this._getUrlParams()
		if (input) {
			this._handleInitialInput(input)
			/* also cleans the current URL from sentitivite/non-permanent params  */
			this._clearUrlParam('input')
		}
	}
	_handleSettings(event) {
		if (event.detail) {
			this.apiKey = event.detail['openai-api-key']
			this.autoAskGPT = event.detail['auto-ask-gpt']
		}
	}
	_handleAudioRecorded(event) {
		this.recording = event.detail;
		this._transcribeRecording();
	}
	_handleTextRecorded(event) {
		const transcription = event.detail;
		this._newLog({
			transcription,
		})
	}
	_handleInitialInput(text) {
		this._newLog({
			transcription: text,
		})
	}
	_transcribeRecording() {
		const speechToText = this.querySelector('speech-to-text');
		if (speechToText && this.recording) {
			speechToText.file = this.recording;
		}
	}

	_handleTranscription(event) {
		const {transcription, audioInput, error} = event.detail;
		this._newLog({
			audioInput,
			transcription,
			error,
		})
	}
	createRenderRoot() {
		return this
	}
}
