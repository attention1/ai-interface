import { LitElement, html } from 'lit';

export default class TextRecorder extends LitElement {
	static get properties() {
		return {
			submit: {type: String}
		};
	}
	getSubmit() {
		return this.submit || "↲"
	}
	constructor() {
		super();
	}
	render() {
		return html`
			<form @submit=${this._onSubmit}>
				<textarea type="text" name="text" placeholder="Send a message"></textarea>
				<button>${this.getSubmit()}</button>
			</form>
		`;
	}
	_onSubmit(event) {
		event.preventDefault()
		const data = new FormData(event.target)
		const text = data.get('text')
		this.dispatchEvent(new CustomEvent('text', { detail: text }));
	}
	createRenderRoot() {
		return this
	}
}
