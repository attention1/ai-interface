/* box sizing model */
html {
	box-sizing: border-box;
}
*, *:before, *:after, {
	box-sizing: inherit;
}

/* variables */
:root {
	--size-font: 20px;
	--size: 1.5rem;
	--size-container: 30rem;
}

@media (prefers-color-scheme: dark) {
	:root,
	:host {
		--color-text: white;
		--color-link: orange;
		--color-background: black;
		--color--background--log-even: slategray;
		--color--background--log-odd: dimgray;
	}
}
@media (prefers-color-scheme: light) {
	:root,
	:host {
		--color-text: black;
		--color-background: white;
		--color-link: blue;
		--color--background--log-even: slategray;
		--color--background--log-odd: lightgray;
	}
}

/* native elements */
:root {
	font-size: var(--size-font);
	min-height: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;
}
body {
	margin: 0;
	padding: 0;
	background-color: var(--color-background);
	color: var(--color-text);
	display: flex;
	flex-direction: column;
	flex-grow: 1;
	max-width: var(--size-container);
	width: 100%;
}

a {
	color: var(--color-link);
}
p,
h1,
h2,
h4 {
	margin-top: 0;
}

button,
input {
	padding: calc(var(--size) / 3);
	background-color: transparent;
	border-color: var(--color-text);
	color: var(--color-text);
	font-size: 1rem;
}
button {
	cursor: pointer;
}
input {}
label {
	font-weight: bold;
}

summary {
	cursor: pointer;
	/* margin-bottom: calc(var(--size) / 3); */
	padding: calc(var(--size) / 3);
}

/* components */
ai-interface {
	display: flex;
	flex-direction: column;
	flex-grow: 1;
}
ai-interface header {
	background-color: var(--color-background);
}
ai-interface main {
	flex-grow: 1;
}

ai-interface footer {
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	background-color: var(--color-background);
	position: sticky;
	bottom: 0;
	z-index: 2;
}
log-list ul {
	padding: 0;
	margin: 0;
	list-style: square;
}
log-list li:nth-child(even) {
	background-color: var(--color--background--log-even);
}
log-list li:nth-child(odd) {
	background-color: var(--color--background--log-odd);
}
log-item {
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	padding: calc(var(--size) / 3);
	margin-bottom: var(--size);
}
log-item-header {
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	margin-bottom: calc(var(--size) / 2);
}
log-item-header audio {
	display: none; /* hidden until know what to do with it */
	width: 100%;
	margin-bottom: calc(var(--size) / 3);
}
log-item-footer {
	display: flex;
	justify-content: flex-end;
}

app-settings {
	display: flex;
	justify-content: flex-end;
	margin-bottom: calc(var(--size) / 2);
}

app-settings details[open] {
	flex-grow: 1;
}

app-settings summary {
	text-align: right;
}

app-settings form {
	display: flex;
	flex-wrap: wrap;
	flex-direction: column;
	justify-content: flex-end;
	align-items: flex-end;
	border: 1px solid var(--color-link);
	padding: calc(var(--size) / 2);
}
app-settings fieldset {
	border: none;
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	width: 100%;
	padding: calc(var(--size) / 4);
}
app-settings form label,
app-settings form button,
app-settings form input {
	margin: calc(var(--size) / 4);
	flex-grow: 1;
}
app-settings menu {
	display: flex;
	flex-wrap: wrap;
	list-style-position: inside;
}
app-settings menu li:not(:first-child) {
	margin-left: calc(var(--size) / 2);
}

audio-recorder {
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
	align-items: flex-end;
	padding: calc(var(--size) / 4);
}
audio-recorder input {
	display: none; /* hidden until know what to do with it */
	width: 100%;
}

audio-recorder button[active] {
	border-color: var(--color-link);
}

text-recorder {
	flex-grow: 1;
}
text-recorder form {
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
}
text-recorder textarea {
	flex-grow: 1;
	background-color: transparent;
	padding: calc(var(--size) / 5);
	color: var(--color-text);
	font-size: 1rem;
}

ask-gpt article {
	font-style: italic;
	white-space: pre-wrap;
	padding-left: calc(var(--size) / 2);
}

ask-gpt button[loading] {
	opacity: 0.5;
	pointer-events: none;
	border-color: transparent;
}

speech-to-text {
	display: flex;
	align-items: center;
}
