import { LitElement, html } from 'lit';

import packageJson from "../package.json"
const packageConfig = packageJson[packageJson.name]

export default class SettingsComponent extends LitElement {
	static get properties() {
		return {
			settings: { type: Object },
		};
	}

	get oaiAPIKey() {
		return this.settings['openai-api-key'] || ''
	}
	get autoAskGPT() {
		return this.settings['auto-ask-gpt'] || true
	}
	get speak() {
		return this.settings['speak'] || false
	}

	connectedCallback() {
		super.connectedCallback();
		this.settings = this.loadSettings();
		this.dispatchEvent(new CustomEvent('settings', { detail: this.settings }));
	}

	loadSettings() {
		const settings = JSON.parse(localStorage.getItem('settings'));
		return settings ? settings : {};
	}

	saveSettings(event) {
		event.preventDefault();
		const form = event.target;
		const settings = {
			'openai-api-key': form['openai-api-key'].value,
			'auto-ask-gpt': form['auto-ask-gpt'].checked,
			'speak': form['speak'].checked,
		};
		localStorage.setItem('settings', JSON.stringify(settings));
		this.settings = settings;
		this.dispatchEvent(new CustomEvent('settings', { detail: this.settings }));
	}

	render() {
		return html`
			<details>
				<summary>
					Settings ${this.oaiAPIKey ? '(connected)' : '(missing key)'}
				</summary>
				${this._renderForm()}
				${this._renderMenu()}
			</details>
		`;
	}
	_renderForm() {
		const oaiKey = html`
			<fieldset>
				<label for="openai-api-key">
					openai-api-key
					(<a href="https://platform.openai.com/account/api-keys" target="_blank">get key</a>)
				</label>
				<input
					type="text"
					id="openai-api-key"
					name="openai-api-key"
					.value="${this.oaiAPIKey}"
					required
					placeholder="open-ai-private-api-key"
				>
			</fieldset>
		`
		const autoAsk = html`
			<fieldset>
				<label for="auto-ask-gpt">auto-ask-gpt</label>
				<i>
					Should sending a message, automatically ask the AI?
					<input type="checkbox" name="auto-ask-gpt" id="auto-ask-gpt" .checked="${this.autoAskGPT}"></input>
				</i>
			</fieldset>
		`
		const speak = html`
			<fieldset>
				<label for="speak">speak</label>
				<p>
					Should the Browser Text2Speech read the AI answer's?
					<input type="checkbox" name="speak" id="speak" .checked="${this.speak || false}"></input>
				</p>
			</fieldset>
		`
		return html`
			<form id="settingsForm" @submit="${this.saveSettings}">
				${oaiKey}
				${autoAsk}
				${speak}
				<fieldset>
					<button type="submit">Save</button>
				</fieldset>
			</form>
		`
	}
	_renderMenu() {
		const {homepage} = packageJson
		const {matrixRoom} = packageConfig
		const $matrix = html`
			<li>
				<a href=${"https://matrix.to/#/" + matrixRoom} target="_blank">
					${matrixRoom}
				</a>
			</li>
		`
		const $homepage = html`
			<li>
				<a href=${homepage} target="_blank">git</a>
			</li>
		`
		if (homepage || matrix) {
			return html`
				<menu>
					${homepage && $homepage}
					${matrixRoom && $matrix}
				</menu>
			`
		}
	}
	createRenderRoot() {
		return this
	}
}
