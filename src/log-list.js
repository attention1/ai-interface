import { LitElement, html } from 'lit';
import {repeat} from 'lit/directives/repeat.js';

export default class LogList extends LitElement {
	static get properties() {
		return {
			apiKey: { type: String },
			logs: { type: Array },
			autoAskGPT: { type: Boolean },
			speak: { type: Boolean, state: true },
		};
	}

	constructor() {
		super();
		this.logs = [];
	}

	attributeChangedCallback(name, oldValue, newValue) {
		if (name === 'logs') {
			this.logs = JSON.parse(newValue);
			this.requestUpdate();
		}
	}

	render() {
		return html`
			<ul>
				${repeat(this.logs, (log) => log.id, (log, index) => this.renderLog(log))}
			</ul>
		`;
	}
	renderLog(log) {
		return html`
			<li>
				<log-item>
					${ this.renderLogHeader(log) }
					${ !log.error ? this.renderAsk(log) : null}
					${ log.error ? this.renderError(log) : null}
			</li>
		`
	}
	renderLogHeader(log) {
		return html`
			<log-item-header>
				${this.renderLogAudio(log)}
				<article>${log.transcription}</article>
				<text-to-speech text=${log.transcription} ?speak=${this.speak}><text-to-speech>
			</log-item-header>
		`
	}
	renderLogAudio(log) {
		if (!log.audioInput) return
		return html`
			<audio controls src="${URL.createObjectURL(log.audioInput)}"></audio>
		`
	}
	renderAsk(log) {
		return html`
			<log-item-footer>
				<ask-gpt
					.apiKey=${this.apiKey}
					.question=${log.transcription}
					.autoAsk=${this.autoAskGPT}
					></ask-gpt>
					</footer>
			</log-item-footer>
		`
	}
	renderError(log) {
		return html`
			<log-item-error>
				<output>${log.error.message}</output>
				<text-to-speech text=${log.error.message} ?speak=${this.speak}></text-to-speech>
			</log-item-error>
		`
	}
	createRenderRoot() {
		return this
	}
}
