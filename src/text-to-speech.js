import { LitElement, html } from 'lit';
import { property, query } from 'lit/decorators.js';

export default class TextToSpeech extends LitElement {
	static get properties() {
		return {
			text: { type: String },
			hasVoices: { type: Boolean },
			speak: { type: Boolean },
		};
	}

	constructor() {
		super();
		this.text = '';
		this.hasVoices = speechSynthesis.getVoices().length > 0
	}

	updated(changedProperties) {
		if (changedProperties.has('text') && this.text) {
		if (!this.speak || !this.text || !this.hasVoices) return false
			this.speech(this.text)
		}
	}

	async speech(text) {
		this.speacking = true
		let utterance = new SpeechSynthesisUtterance(text)
		speechSynthesis.speak(utterance)
		this.speacking = false
	}

	render() {
		return this.speacking ? (
			html`♫)))`
		) : null
	}

	createRenderRoot() {
		return this
	}
}
