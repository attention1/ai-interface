import { LitElement, html } from "lit";

const DEFAULT_GPT_PROMPT = {
	role: "system",
	content: "You are a helpful assistant."
}

export default class AskGPT extends LitElement {
	static get properties() {
		return {
			apiKey: { type: String },
			question: { type: String },
			response: { type: String, reflect: true, state: true },
			autoAsk: { type: Boolean, state: true },
			loading: { type: Boolean, state: true },
		};
	}

	constructor() {
		super();
		this.question = "";
		this.response = "";
		this.autoAsk = false;
		this.loading = false;
	}

	connectedCallback() {
		super.connectedCallback();
		if (this.question && this.autoAsk) {
			this.complete();
		}
	}

	render() {
		return html`
			${!this.response ? this.renderButton() : this.renderResponse()}
		`;
	}
	renderButton() {
		if (this.autoAsk && !this.response) {
			return html`
				<button @click="${this.handleGPT}" ?loading=${this.loading}>…</button>
			`;
		} else {
			return html` <button @click="${this.handleGPT}" ?loading=${this.loading}>
				${!this.loading ? "Ask ChatGPT" : "…"}
			</button>`;
		}
	}
	renderResponse() {
		return html`<article>${this.response}</article>`;
	}

	async handleGPT({ target }) {
		this.complete();
	}
	async complete() {
		this.loading = true;
		const messages= [
			{...DEFAULT_GPT_PROMPT},
			{
				role: "user",
				content: this.question
			}
		]
		let data;
		try {
			/* https://platform.openai.com/docs/api-reference/chat */
			const response = await fetch(
				"https://api.openai.com/v1/chat/completions",
				{
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						Authorization: `Bearer ${this.apiKey}`,
					},
					body: JSON.stringify({
						messages,
						max_tokens: 1000,
						model: "gpt-3.5-turbo",
					}),
				}
			);
			data = await response.json();
		} catch (error) {
			console.log("error getting chatGPT answer", error);
		}
		if (data && data.choices) {
			this.response = data.choices[0].message.content.trim();
		}
		this.loading = false;
	}
	createRenderRoot() {
		return this;
	}
}
