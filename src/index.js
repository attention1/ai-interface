import AIInterface from './ai-interface.js'
import AudioRecorder from './audio-recorder.js'
import TextRecorder from './text-recorder.js'
import SpeechToText from './speech-to-text.js'
import TextToSpeech from './text-to-speech.js'
import AppSettings from './app-settings.js'
import LogList from './log-list.js'
import AskGPT from './ask-gpt.js'

window.customElements.define('ai-interface', AIInterface)
window.customElements.define('audio-recorder', AudioRecorder)
window.customElements.define('text-recorder', TextRecorder)
window.customElements.define('speech-to-text', SpeechToText)
window.customElements.define('text-to-speech', TextToSpeech)
window.customElements.define('app-settings', AppSettings)
window.customElements.define('log-list', LogList)
window.customElements.define('ask-gpt', AskGPT)

export {
	AIInterface,
	AudioRecorder,
	TextRecorder,
	SpeechToText,
	AppSettings,
	LogList,
	AskGPT,
	TextToSpeech,
}
