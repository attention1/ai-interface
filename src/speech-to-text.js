import { LitElement, html } from 'lit';
import { property, query } from 'lit/decorators.js';

export default class SpeechToText extends LitElement {
	static get properties() {
		return {
			apiKey: { type: String },
			file: { type: Object },
			transcription: { type: String },
			transcribing: { type: Boolean },
		};
	}

	constructor() {
		super();
		this.apiKey = '';
		this.file = null;
		this.transcription = '';
		this.transcribing = false;
	}

	updated(changedProperties) {
		if (changedProperties.has('file') && this.file) {
			this._transcribeFile(this.file);
		}
	}

	async _transcribeFile(file) {
		this.transcribing = true
		const url = 'https://api.openai.com/v1/audio/transcriptions';
		const headers = new Headers({
			'Authorization': `Bearer ${this.apiKey}`,
			/* not needed (as in spec), breaks with 404 */
			/* 'Content-Type': 'multipart/form-data', */
		});
		const formData = new FormData();
		formData.append('file', file);
		formData.append('model', 'whisper-1');

		let transcriptionData,
				transcriptionError;
		try {
			const response = await fetch(url, {
				method: 'POST',
				headers,
				body: formData
			});
			const res = await response.json();
			const {error, text} = res
			if (error) throw error
			transcriptionData = text
		} catch(error) {
			transcriptionError = error
		}
		let detail = {}
		if (transcriptionData) {
			this.transcription = transcriptionData.trim();
			detail.transcription = this.transcription;
		}

		if (transcriptionError) {
			detail.error = transcriptionError
		}

		detail.audioInput = file
		this.dispatchEvent(new CustomEvent('transcription', { detail }));
		this.transcribing = false
	}

	render() {
		return this.transcribing ? (
			html`♫`
		) : null
	}

	createRenderRoot() {
		return this
	}
}
